
const mongoose = require('mongoose');
const Dna = mongoose.model('Dna');


exports.listDna = async (req, res) => {
  try {
    const data = await Dna.find({});
    res.status(200).send(data);
  } catch (e) {
    res.status(500).send({ message: 'Falha ao carregar Dna.' });
  }

};


exports.createDna = async (req, res) => {

  const dna = new Dna({
    dna: req.body.dna
  }
  );
  dna.save();
  const d1 = dna.dna;
  d1.map(async function (item) {
    if (item.includes('CCCC') | item.includes('AAAA') | item.includes('TTTT') | item.includes('GGGG')) {

      await res.status(200).send({ message: 'HTTP 200-OK' });
    }
  })
  return res.status(400).send({ message: 'HTTP 403-FORBIDDEN' });


}

exports.product_delete = function (req, res) {
  Dna.findByIdAndRemove(req.params.id, function (err) {
    if (err) return next(err);
    res.send('Deleted successfully!');
  })
};